<?php

function analyseCommande($line, &$commandLine, &$command_args, &$command_options)
{
    $matches = [];
    $estValide = preg_match("#^([A-Za-z]\w*) *((?:(?: +(?:(?:-[A-Za-z]+)|(?:\.?\.?\/?\"?[A-Za-z0-9]+\"?(?:[\.\\\\/\-_]*[A-Za-z0-9]+)*)))+)*)$#", $line, $matches);
    if ($estValide) {
        $commandLine = $matches[1];
        $subject = explode(" ", $matches[2]);
        $patternOptions = '#^-([A-Za-z]+)$#';
        $patternArgs = '#^[^\-]\.?\.?\/?\"?[A-Za-z0-9]+\"?([\.\\\\/\-_]*[A-Za-z0-9]+)*$#';
        $replaceArgs = '$0';
        $replaceOptions = '$1';
        $command_options_tmp = preg_filter($patternOptions, $replaceOptions, $subject);
        $command_args = preg_filter($patternArgs, $replaceArgs, $subject);
        $command_options = [];
        foreach ($command_options_tmp as $option) {;
            $command_options = array_merge($command_options, str_split($option));
        }
    } else {
        echo ("n'est pas valide");
    }
}
function lireRepertoire($dirPath)
{
    $allFiles = [];
    if (!preg_match("#[/\\\\]\.\.?$#", $dirPath)) {
        if (is_dir($dirPath)) {
            $dirContent = scandir($dirPath);
            foreach ($dirContent as $value) {
                if (is_dir($dirPath . "/" . $value)) {
                    if (!preg_match("#^\.+$#", $value)) {
                        $allFiles["$dirPath/$value"] = lireRepertoire($dirPath . "/" . $value);
                    }
                } else {
                    $allFiles[] = $dirPath . "/" . $value;
                }
            }
        }
    }
    return $allFiles;
}
function MyLs($dirPath, $command_options, $command_args)
{
    if (!empty($command_options) && !empty($command_args)) {
        $option = $command_options[0];
        $argument = $command_args[0];
        if ($option == "l") $dirPath = $dirPath . "/" . $argument;
    }
    $repertoire = (lireRepertoire($dirPath));
    echo ("\n");
    echo (" type                          lastWriteTime                      Lenght      Name \n");
    echo (" ----                          -------------                      ------      ---- \n");
    foreach ($repertoire as $key => $value) { // ext.

        if (is_array($value)) {
            echo (" -r---                         " . date("d/m/y H:i", filemtime("$key") + 3600) . "                     " . str_pad(filesize("$key"), 6, " ", STR_PAD_LEFT) . "      " . str_pad(basename("$key"), 20, " ", STR_PAD_RIGHT) . " \n");
        } elseif (!is_array($value)) {
            echo (" --f--                         " . date("d/m/y H:i", filemtime($value) + 3600) . "                     " . str_pad(filesize($value), 6, " ", STR_PAD_LEFT) . "      " . str_pad(basename($value), 20, " ", STR_PAD_RIGHT) . " \n");
        }
    }
}
function Mypwd($dirPath)
{
    echo "\n";
    echo (" Path");
    echo "\n";
    echo (" ----");
    echo "\n";
    echo ($dirPath);
    echo "\n";
    echo "\n";
}
function Myrm($dirPath, $command_args, $command_options)
{
    if (preg_match("#^\.\.?\/.*#", $command_args[0])) $dirPath = $command_args[0];
    else {
        $dirPath = $dirPath . "/" . $command_args[0];
    }
    if (!empty($command_options[0]) == "r") {
        if (is_dir($dirPath)) {
            $tab = scandir($dirPath);
            if (count($tab) <= 2) {
                rmdir($dirPath);
            } else {
                echo "\n";
                echo (basename($dirPath) . " n'est pas vide !!! ");
                return;
            }
        }
        if (is_file($dirPath)) {
            unlink($dirPath);
        }
    } elseif (empty($command_options[0])) {
        if (is_dir($dirPath)) {
            echo "\n";
            echo basename($dirPath) . " est un dossier";
            return;
        }
        if (!is_dir($dirPath)) {
            unlink($dirPath);
        }
    } else echo "\n ERROR";
    return;
}
function Mymkdir($dirPath, $command_args, $command_options)
{
    if (preg_match("#^\.\.?\/.*#", $command_args[0])) $dirPath = $command_args[0];
    else {
        $dirPath = $dirPath . "/" . $command_args[0];
    }
    if (file_exists($dirPath)) {
        echo "Le fichier $dirPath existe.";
    } else {
        mkdir($dirPath);
    }
}
function Myps()
{
    $tab = shell_exec("tasklist");
    print_r($tab);
}
function Mycd($dirpath, $path)
{

    $pattern=strpos(strtoupper($_SERVER['OS']), 'WINDOW')===0?"#[a-z]:/?.*#i":"#/.*#";

    if(preg_match($pattern, $path)){
        if(!file_exists($path)){
            return false;
        }
        return $path;
    }

    $dirpath = str_replace("\\","/",$dirpath );
    $path =str_replace("\\","/",$path);
    $partieschemin = explode("/",$path);

    foreach ($partieschemin as $partie){
        if(preg_match("#^\.{2}$#i",$partie)){
            $dirpath=dirname($dirpath);
        }    
        elseif (!preg_match("#^\.?$#i",$partie)){
            $dirpath="/".$partie;
            $dirpath = str_replace("//","/",$dirpath);
        }
    }

    if (!file_exists($dirpath)){
        return false;
    }

    return $dirpath;
}
function Myhelp()
{
    echo "\n";
    echo ("--------------------------------   MYHELP  ----------------------------------- \n");
    echo ("\n - Myls : \n");
    echo ("        Afficher le contenu du dossier en cours sous le format suivant. \n");
    echo ("\n");
    echo ("    [Mode] ---------------- [lastWritTime] ------------------------- [lenght] -- [Nom]\n");
    echo ("\nOption(s) :\n");
    echo ("  - l : Affiche le dossier d'un chemin spécifié \n");
    echo ("\n - Mypwd : \n");
    echo ("        Affiche le chemin du dossier en cours.\n");
    echo (" Option(s) : Aucune.\n");
    echo ("\n - Myrm : \n");
    echo ("        Supprime un fichier ou un dossier '-r' dans un chemin spécifié.\n");
    echo (" Option(s) : \n");
    echo ("  - r : Supprime un dossier (avec ou sans '-r' ou sans la suppression de fichier est possible)\n");
    echo ("\n - Mymkdir : \n");
    echo ("        Crée un dossier Dans un chamin spécifié\n");
    echo (" Option(s) : Aucune.\n");
    echo ("\n - Myps : \n");
    echo ("        Affiche les processus en cours d'execution sur l'ordinateur sous le format suivant.\n");
    echo ("\n");
    echo ("    [Nom de l’image] --- [PID (Process IDentifier)] --- [Nom de la session] -- [Numéro de session] -- [Utilisation]");
    echo ("\n\n");
    echo ("\n - Myhelp : \n");
    echo ("        Affiche les commandes disponibles.");
    echo ("\n\n");
}

//echo(date("d/m/y H:i",filemtime("C:\EnvDev\GIT\projet_mini_shell\MesFonctions.php")+3600));