<?php

function Mymv($command_args)
{
    print_r($command_args);
    $dirpath1 = $command_args[0];
    $dirpath2 = $command_args[1];



    if (is_dir($dirpath1)) {
        copydir($dirpath1, $dirpath2, $dirpath1);
        supprDossierplein($dirpath1,$command_args);
        supprDossierplein2($dirpath1,$command_args);
    } elseif (is_file($dirpath1)) {
        if (file_exists($dirpath2)) {
            echo "le fichier existe déja dans la destination ";
            return 0;
        }
        copy("$dirpath1", "$dirpath2");
        unlink("$dirpath1");
    } else {
        echo (" Erreur !! ");
        return 0;
    }
    return $dirpath1;
}
function copydir($origine, $destination, $dirpath1)
{
    $dossier = opendir($origine);
    if (file_exists($destination)) {
        echo (" Le dossier existe déja dans la destination ");
        return 0;
    }
    mkdir($destination);
    $total = 0;
    while ($fichier = readdir($dossier)) {
        $l = array('.', '..');
        if (!in_array($fichier, $l)) {
            if (is_dir($origine . "/" . $fichier)) {
                $total += copydir("$origine/$fichier", "$destination/$fichier", $dirpath1);
            } else {
                copy("$origine/$fichier", "$destination/$fichier");
                $total++;
            }
        }
    }
}
function supprDossierplein($dirpath1,$command_args)
{
    $command_options[0]="r";
    if (is_dir($dirpath1)) { // si le paramètre est un dossier
        $objects = scandir($dirpath1); // on scan le dossier pour récupérer ses objets
        foreach ($objects as $object) { // pour chaque objet
            if ($object != "." && $object != "..") { // si l'objet n'est pas . ou ..
                if (is_dir($dirpath1 . "/" . $object) && !empty($dirpath1 . "/" . $object)) {
                    supprDossierplein($dirpath1 . "/" . $object,$command_args);
                    
                } elseif (is_file($dirpath1 . "/" . $object)) {
                    unlink($dirpath1 . "/" . $object);
                }
            }
        }
         // on supprime le dossier
    }
}
function supprDossierplein2($dirpath1,$command_args)
{
    $command_options[0]="r";
    if (is_dir($dirpath1)) { // si le paramètre est un dossier
        $objects = scandir($dirpath1); // on scan le dossier pour récupérer ses objets
        foreach ($objects as $object) { // pour chaque objet
            if ($object != "." && $object != "..") { // si l'objet n'est pas . ou ..
                if (is_dir($dirpath1 . "/" . $object) && !empty($dirpath1 . "/" . $object)) {
                    supprDossierplein($dirpath1 . "/" . $object,$command_args);
                } elseif (is_file($dirpath1 . "/" . $object)) {
                    unlink($dirpath1 . "/" . $object);
                }if (is_dir($dirpath1 . "/" . $object) && empty($dirpath1 . "/" . $object)) {
                    rmdir($dirpath1 . "/" . $object);
                }

            }
        }
         // on supprime le dossier
    }
}
