<?php

function Mytail($command_args, $command_option)
{
    echo ("\n");
    if (!empty($command_option_Mytail) && file_exists($command_args[0])) {
        preg_match("#\d+#", $command_option_Mytail[1], $command_options_Mytail);
        $fichier = file($command_args[0]);
        $total = count($fichier) - 1;
        if ($total < $command_options_Mytail[0]) {
            echo (" Le fichier contient que ".($total+1)." ligne. \n\n");
        }
        if (($handle = fopen($command_args[0], "r")) !== FALSE) {
            if ($total == 0) {
                echo (" Votre fichier est vide \n");
                return 0;
            } elseif ($total >= $command_options_Mytail[0]) {
                for ($i = ($total - $command_options_Mytail[0]+1); $i <= $total; $i++) {
                    echo $fichier[$i];
                }
            } elseif ($total < $command_options_Mytail[0]) {
                for ($i = 0; $i <= $total; $i++) {
                    echo $fichier[$i];
                }
            }
        }
        return 0;
    } elseif (empty($commandes_options) && file_exists($command_args[0])) {
        $fichier = file($command_args[0]);
        $total = count($fichier) - 1;
        if (($handle = fopen($command_args[0], "r")) !== FALSE) {
            if ($total == 0) {
                echo (" Votre fichier est vide \n");
                return 0;
            } elseif ($total >= 10) {
                for ($i = ($total - 9); $i <= $total; $i++) {
                    echo $fichier[$i];
                }
            } elseif ($total < 10) {
                for ($i = 0; $i <= $total; $i++) {
                    echo $fichier[$i];
                }
            }
        }
    }elseif (!file_exists($command_args[0])){
        echo (" Fichier introuvable \n ");
    }
}
