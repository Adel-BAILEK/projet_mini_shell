<?php

// function Mycd($dirpath, $path)
// {
//     $pattern = strpos(strtoupper($_SERVER['OS']), 'WINDOW') === 0 ? "#[a-z]:/?.*#i" : "#/.*#";
//     if (preg_match($pattern, $path)) {
//         if (!file_exists($path)) {
//             return false;
//         }
//         return $path;
//     }
//     $dirpath = str_replace("\\", "/", $dirpath);
//     $path = str_replace("\\", "/", $path);
//     $partieschemin = explode("/", $path);
//     foreach ($partieschemin as $partie) {
//         if (preg_match("#^\.{2}$#i", $partie)) {
//             $dirpath = dirname($dirpath);
//         } elseif (!preg_match("#^\.?$#i", $partie)) {
//             $dirpath = "/" . $partie;
//             $dirpath = str_replace("//", "/", $dirpath);
//         }
//     }
//     if (!file_exists($dirpath)) {
//         return false;
//     }
//     return $dirpath;
// }
function calculerChemin(string &$dirPath, string $chemindestination)
{
    //si windows 
    if (getOS() === "Windows") {
        $patternAbsolu = "#^[a-z]:.*$#i";
    } else {
        $patternAbsolu = "#^/.*$#i";
    }
    $dirPath = str_replace("\\", "/", $dirPath);
    $chemindestination = str_replace("\\", "/", $chemindestination);
    if (preg_match($patternAbsolu, $chemindestination)) {
        if (!file_exists($chemindestination))
            return false;
        return $chemindestination;
    }
    
    $partieChemin = explode("/", $chemindestination);
    foreach ($partieChemin as $unePartie) {
        if (preg_match("#^\.{2}.*#", $unePartie)) {
            $dirPath = dirname($dirPath, 1);
        } else if (preg_match("#^[^.]+$#i", $unePartie)) {
            $dirPath = $dirPath . "/" . $unePartie;  
        }
        $dirPath = str_replace("\\", "/", $dirPath);
        $dirPath = str_replace("//", "/", $dirPath);
    }

    if (!file_exists($dirPath))
        return false;

    return $dirPath;
}
