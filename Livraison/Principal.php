<?php


include "Mycd.php";
include "Myfind.php";
include "Myls.php";
include "Myhelp.php";
include "Commands.php";
include "Mymkdir.php";
include "Myps.php";
include "Mypwd.php";
include "Myrm.php";
include "Mymv.php";
include "Mytail.php";
include "helpers.php";

$dirPath = __DIR__;

echo ("\n");
echo (" Bienvenue dans le Minishell ");
$quitter = "non";
while ($quitter == "non") {
    echo ("\n\n");
    $Line = readline("PS " . $dirPath . "> ");
    analyseCommande($Line, $commandLine, $command_args, $command_options, $est_decoupage_options = true);
    if ($commandLine == "Myls") {
        MyLs($dirPath, $command_options, $command_args);
    } elseif ($commandLine == "Mypwd") {
        Mypwd($dirPath);
    } elseif ($commandLine == "Myrm") {
        Myrm($dirPath, $command_args, $command_options);
    } elseif ($commandLine == "Mymkdir") {
        $nom = $command_args[0];
        $existe = $nom;
        create_dir($nom,$command_options,$existe);
    } elseif ($commandLine == "Myps") {
        Myps();
    } elseif ($commandLine == "Mycd") {
        calculerChemin($dirPath, $command_args[0]);
    } elseif ($commandLine == "Myhelp") {
        Myhelp();
    } elseif ($commandLine == "Mymv") {
        Mymv($command_args);
    } elseif ($commandLine == "Mytail") {
        Mytail($command_args, $command_option_Mytail);
    } elseif ($commandLine == "Q" || $commandLine == "q") $quitter = "oui";
}
