<?php

function Mycp(string $dirPath, $destinationPath)
{
    $resultats=[];
    $destination= $destinationPath;
    if (! file_exists($destination)) {
        echo "création du dossier: $destination"."\n";
        mkdir($destination, 0777, true);
    }
    
    if (is_dir($dirPath)) {
        $dirHandle = opendir($dirPath);
        while (($item = readdir($dirHandle)) != false) {
            if (!preg_match("#[/\\\\]?\.\.?$#", $item)) {
                //if windows
                $fichierAvecChemin = $dirPath . "\\" . $item;
                if (is_dir($fichierAvecChemin)) {
                    $destination= $destinationPath."\\". $item;
                    if (! file_exists($destination)) {
                        echo "création du dossier: $destination"."\n";
                        mkdir($destination, 0777, true);
                    }
                    $donneesSousRepertoireEnCours = Mycp($fichierAvecChemin, $destination);
                } else {
                    // echo "dans la fonction isfile";
                    copy($fichierAvecChemin,$destinationPath."\\".$item);
                    echo("copie :". $destination."\\".$item."\n"); 
                }
            }
        }
        closedir($dirHandle);
    }else{
        copy($dirPath,$destination);
        echo("copie :". $destination); 
    }
    
}
$dirPath=readline("Entrez la source :");
$destinationPath=readline("Entrez la destination :");
Mycp($dirPath, $destinationPath);
