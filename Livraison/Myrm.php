<?php
function Myrm($dirPath, $command_args, $command_options)
{
    if (preg_match("#^\.\.?\/.*#", $command_args[0])) $dirPath = $command_args[0];
    else {
        $dirPath = $dirPath . "/" . $command_args[0];
    }
    if (!empty($command_options[0]) == "r") {
        if (is_dir($dirPath)) {
            $tab = scandir($dirPath);
            if (count($tab) <= 2) {
                rmdir($dirPath);
            } else {
                echo "\n";
                echo (basename($dirPath) . " n'est pas vide !!! ");
                return;
            }
        }
        if (is_file($dirPath)) {
            unlink($dirPath);
        }
    } elseif (empty($command_options[0])) {
        if (is_dir($dirPath)) {
            echo "\n";
            echo basename($dirPath) . " est un dossier";
            return;
        }
        if (!is_dir($dirPath)) {
            unlink($dirPath);
        }
    } else echo "\n ERROR";
    return;
}

?>