<?php

function MyLs($dirPath, $command_options, $command_args)
{
    if (!empty($command_options) && !empty($command_args)) {
        $option = $command_options[0];
        $argument = $command_args[0];
        if ($option == "l") $dirPath = $dirPath . "/" . $argument;
    }
    $repertoire = (lireRepertoire($dirPath));
    echo ("\n");
    echo (" type                          lastWriteTime                      Lenght      Name \n");
    echo (" ----                          -------------                      ------      ---- \n");
    foreach ($repertoire as $key => $value) { // ext.

        if (is_array($value)) {
            echo (" -r---                         " . date("d/m/y H:i", filemtime("$key") + 3600) . "                     " . str_pad(filesize("$key"), 6, " ", STR_PAD_LEFT) . "      " . str_pad(basename("$key"), 20, " ", STR_PAD_RIGHT) . " \n");
        } elseif (!is_array($value)) {
            echo (" --f--                         " . date("d/m/y H:i", filemtime($value) + 3600) . "                     " . str_pad(filesize($value), 6, " ", STR_PAD_LEFT) . "      " . str_pad(basename($value), 20, " ", STR_PAD_RIGHT) . " \n");
        }
    }
}
function lireRepertoire($dirPath)
{
    $allFiles = [];
    if (!preg_match("#[/\\\\]\.\.?$#", $dirPath)) {
        if (is_dir($dirPath)) {
            $dirContent = scandir($dirPath);
            foreach ($dirContent as $value) {
                if (is_dir($dirPath . "/" . $value)) {
                    if (!preg_match("#^\.+$#", $value)) {
                        $allFiles["$dirPath/$value"] = lireRepertoire($dirPath . "/" . $value);
                    }
                } else {
                    $allFiles[] = $dirPath . "/" . $value;
                }
            }
        }
    }
    return $allFiles;
}




?>