<?php

function Myhelp()
{
    echo "\n";
    echo ("--------------------------------   MYHELP  ----------------------------------- \n");
    echo ("\n - Myls : \n");
    echo ("        Afficher le contenu du dossier en cours sous le format suivant. \n");
    echo ("\n");
    echo ("    [Mode] ---------------- [lastWritTime] ------------------------- [lenght] -- [Nom]\n");
    echo ("\nOption(s) :\n");
    echo ("  - l : Affiche le dossier d'un chemin spécifié \n");
    echo ("\n - Mypwd : \n");
    echo ("        Affiche le chemin du dossier en cours.\n");
    echo (" Option(s) : Aucune.\n");
    echo ("\n - Myrm : \n");
    echo ("        Supprime un fichier ou un dossier '-r' dans un chemin spécifié.\n");
    echo (" Option(s) : \n");
    echo ("  - r : Supprime un dossier (avec ou sans '-r' ou sans la suppression de fichier est possible)\n");
    echo ("\n - Mymkdir : \n");
    echo ("        Crée un dossier Dans un chamin spécifié\n");
    echo (" Option(s) : Aucune.\n");
    echo ("\n - Myps : \n");
    echo ("        Affiche les processus en cours d'execution sur l'ordinateur sous le format suivant.\n");
    echo ("\n");
    echo ("    [Nom de l’image] --- [PID (Process IDentifier)] --- [Nom de la session] -- [Numéro de session] -- [Utilisation]");
    echo ("\n\n");
    echo ("\n - Myhelp : \n");
    echo ("        Affiche les commandes disponibles.");
    echo ("\n\n");
}



?>